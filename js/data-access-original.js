// JavaScript Document

/*
 * ---
 * general modal controls
 * ---
 */


function showModal(id) {
	$('#page-mask').show();
	$('#'+id).show();
	$('#'+id+' input').first().focus();
}
function modalClose(id) {
	$('#page-mask').hide();
	if (id) {
		$('#'+id).hide();
	} else {
		$('.modal').hide();
	}
}

/* 
 * --- 
 * Registration form checker 
 * ---
 */
$(document).ready(function(){
	// adds highlight to focused field, and highlights help text 
	$('.inputWrapper input').focusin( function(){
		$(this).parent().addClass('highlighted');
		$(this).next().removeClass('hidden');
	});
	// removes highlight and hides help text
	$('.inputWrapper input').focusout( function(){
		$(this).parent().removeClass('highlighted');
		$(this).next().addClass('hidden');
	});
	// validates username input 
	$('#reg-div input[name="user"]').focusout( function(){
		$(this).val($(this).val().toLowerCase());
	});
	// validates password complexity -- NEEDS FIXING
	$('#reg-div input[name="password"]').focusout( function(){
		var string = $(this).val();
		if ((string) && ( string.match(/[a-zA-Z]/)) && (string.match(/(\d|\W)/)) && (string.length >= 8)) {
			$('#password-simple').addClass('hidden');
			$(this).parent().removeClass('error');
		} else {
			$('#password-simple').removeClass('hidden');
			$(this).parent().addClass('error');
		}
	});
	// validates password match
	$('#reg-div input[name="confirm"]').focusout( function(){
		var string = $(this).val();
		var matchedString = $('#reg-div input[name="password"]').val();
		if (string === matchedString) {
			$('#password-mismatch').addClass('hidden');
			$(this).parent().removeClass('error');
		} else {
			$('#password-mismatch').removeClass('hidden');
			$(this).parent().addClass('error');
		}
	});
			
	// validates email input
	$('#reg-div input.email').focusout( function(){
		var string=$(this).val().toLowerCase();
		if ((string) && ( !string.match(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/) )) {
			$(this).parent().addClass('error');
			$('#email-error').removeClass('hidden');
		} else {
			$('#email-error').addClass('hidden');
			$(this).parent().removeClass('error');
		}
	});
	
	// validates form completion -- NEEDS FIXING
	$('.inputWrapper input').blur( function() {
		var formComplete = true;
		$('.inputWrapper input.required').each(function() {
			var string=$(this).val();
			if (!string) {
				formComplete = false;
				return false;
			}
		});
		if (formComplete) {
			$('#regSubmit').removeProp('disabled');
		}
	}); 
});

/* 
 * --- 
 * Registration thank you and confirmation control 
 * ---
 */

function showThanks() {
	var username = $('#reg-div input[name="user"]').val();
	var email = ($('#reg-div input.email').val()) ? $('#reg-div input.email').val() : 'empty';
	alert(username + ' ' + email);
	
	$('#reg-thanks-msg').append('You have created an account with the username "'+ username +'." Please check your email at '+ email +' to complete your registration.');
	
	$('#reg-div').addClass("hidden");
	$('#reg-thanks-div').removeClass("hidden").show();
	return false;
}
function showConfirm() {
	var username = $('#reg-div input[name="user"]').val();
	var email = ($('#reg-div input.email').val()) ? $('#reg-div input.email').val() : 'empty';
	alert(username + ' ' + email);
	
	$('#reg-confirm-msg').append('Thank you for registering an HCP account with the email address ' + email + '. You have created an account with the username "'+ username +'."');
	
	$('#reg-div').addClass("hidden");
	$('#reg-confirm-div').removeClass("hidden").show();
	return false;
}

/* 
 * --- 
 * validate username / password reset form
 * currently set to disable form and highlight errors
 * ---
 */
 
function forgotValidation() {
	var mailUser=$("#forgotUsername").val(); 
	var resetPass=$("#forgotPassword").val();
	if (!mailUser && !resetPass) {
		alert("Please enter values");
		return false;
	}
	if (mailUser) {
		$("#pass-reminder-error")
			.empty()
			.append("<strong>Error: </strong>The email address you entered cannot be found in the HCP database.")
			.removeClass("hidden");
		$("#forgotUsername")
			.focus();
	}
	if (resetPass) {
		$("#pass-reminder-error")
			.empty()
			.append("<strong>Error: </strong>The username you entered cannot be found in the HCP database.")
			.removeClass("hidden");
		$("#forgotPassword")
			.focus();
	}
	return false;
}

/* 
 * --- 
 * Data Use Terms controls 
 * ---
 */

 // write script that enables checkbox when user scrolls down (i.e. "reads") the terms of use. 
 
function agreeChange(checkbox,id) {
	// if user checks box, don't let them uncheck. Turn on the form submit and guide them to it. 
	$(checkbox).prop('disabled','disabled');
	$('#'+id).removeProp('disabled').focus();
	
}

function dutConfirm() {
	/* 
	 * Change the page according to their acceptance of terms.
	 * Would this require a page refresh in XNAT?
	 */
	  
	$('#dut-Phase2OpenAccess').addClass("hidden");
	$('#dut-thanks-div').removeClass("hidden").show();
	$('#reg-thanks-msg').append("You have accepted the Data Use Terms for this data set, and now have permission to download this data. Please consult documentation on how to download HCP data if you have any questions.");
	
	// hide signup-screen and show data info
	$('#list-Phase2OpenAccess').find('.permission-needed').addClass("hidden");
	$('#list-Phase2OpenAccess').find('dl').removeClass("hidden");
	
	// expose content that would be hidden before DUT acceptance
	$('#list-Phase2OpenAccess').find('.permission-controlled')
		.removeClass("hidden")
		.css('background-color','yellow');
	
	// highlight acceptance of terms
	$('#list-Phase2OpenAccess .dut')
		.addClass("accepted")
		.append(" Accepted")
		.css('background-color','yellow');
	
	// temporarily retrun false to block form processing. 
	return false;
}

