/*
 * set of scripts fo filter list of publications by author.
 * depends on containsNC() and aintContainsNC() functions, which are defined in globals.js
 */
function getUrlHash() {
    // look for hashtag after the URL. Remove the URL itself.
    var parts = window.location.href.split("#");
    // parts=parts.shift();
    return parts[1];
}
function filterByAuthor(author) {
    var counter=0;
    $('.Inv .authors:aintContainsNC('+author+')').parent('.Inv').slideUp('fast');
    $('.Inv .authors:containsNC('+author+')').parent('.Inv')
        .each( function(i) {
            counter++;
        })
        .removeClass("hidden")
        .find('dt').css('background-color','#fffcc0');

    // display count of results
    $('#byAuthor').parent()
        .after("<p id='filterResult'>&raquo; "+((counter > 1) ? counter+" publications" : ((counter >0) ? "One publication" : "No publications")) +" found. [<a href='#' onclick='javascript:resetList()'>Show all publications</a>]</p>");
}
function initializeList(){
    $('.Inv').each(function() {
        $(this).find('dt').css('background-color','transparent');
    }).show();
    $('#filterResult').remove();
}
function resetList(){
    $('#byAuthor option').removeProp('selected');
    $('#byAuthor option:first').prop('selected',true);
    initializeList();
}


$(document).ready( function(){
    /*
     * ---
     * Look for an author selection and hide any entry that does not match. Highlight ones that do.
     * ---
     */
    $('#searchToggle').on('click',function(){
        // initialize list
        initializeList();

        // check for match, while defaulting to showing all
        var author = $('input[name=authorNameSearch]').val();
        if (author.length > 0) {
            filterByAuthor(author);
        } else {
            resetList();
            alert("No author name entered to search for.")
        }
    });

    $('#resetList').on('click',function(){
        // reset list
        $('input[name=authorNameSearch]').val('');
        resetList();
    })

    var hash = getUrlHash();
    if (hash.length > 0) {
        $('#byAuthor').val(hash);
        filterByAuthor(hash);
    }

});