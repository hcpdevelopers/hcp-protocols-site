// JavaScript Document

var initRegCallback={
	success:function(o){
		regSuccess(o);
			},
	failure:function(o){
		if (o.responseText.toLowerCase().indexOf("agree to the data use terms")>=0) {
			showTerms();
		} else {
			regFailed(o);
		}
	},
	scope:this
};
var regCallback={
	success:function(o){
		regSuccess(o);
			},
	failure:function(o){
		regFailed(o);
	},
	scope:this
};

function regFailed(o) {
		if (o.responseText != null && o.responseText.length<100) {
			document.getElementById("reg-error-bar").innerHTML=o.responseText;
		} else {
			document.getElementById("reg-error-bar").innerHTML="The server could not process the registration"
		}
		document.getElementById("reg-error-bar").style.display="block";
		document.getElementById("reg-error-bar").style.visibility="visible";
		ebNotify("Registration could not be completed");
		Recaptcha.reload();
		showReg();
}
function regSuccess(o) {
		ebNotify(o.responseText);
		regClose();
		document.getElementById("username").value=document.getElementById("reg-username").value;
		if (o.responseText.indexOf("Accepted")>0) {
			ebNotify("Registration accepted.  Proceeding with login....");
			document.getElementById("password").value=document.getElementById("reg-pw").value;
			document.getElementById("username").readOnly="true";
			document.getElementById("password").readOnly="true";
			document.getElementById("form1-submit").disabled=="true";
			document.getElementById("form1").submit();
		} else {
			document.getElementById("password").focus();
		}
}
function ebNotify(txt) {
	document.getElementById("error-bar").style.display="block";
	document.getElementById("error-bar").style.visibility="visible";
	document.getElementById("error-bar").innerHTML=txt;
}
function register() {
	document.getElementById("reg-error-bar").style.display="none";
	document.getElementById("reg-error-bar").style.visibility="hidden";
	showReg();
}
function showReg() {
//	document.getElementById("reg-frame").src=document.getElementById("reg-frame").src;
	document.getElementById("page-mask").style.display="block";
	document.getElementById("reg-div").style.display="block";
	window.scrollTo(0,0);
//	document.getElementById("agree-div").style.display="none";
//	document.forms['regForm'].elements['reg-username'].focus();
}
function modalClose(id) {
	$("#page-mask").addClass("hidden");
	$(id).addClass("hidden");
}

function regClose() {
	document.getElementById("page-mask").style.display="none";
	document.getElementById("reg-div").style.display="none";
//	document.getElementById("agree-div").style.display="none";
	closeLightbox();
}
function forgot() {
/*	document.getElementById("forgot-frame").src=document.getElementById("forgot-frame").src;
	document.getElementById("page-mask").style.display="block";
	document.getElementById("forgot-div").style.display="block";
	*/
	document.getElementById("loginForm").style.display="none";
	document.getElementById("forgotForm").style.display="block";
	// document.getElementById("registerButton").style.display="none";
	document.getElementById("forgotButton").style.display="none";
	
}
function forgotClose() {
/*	document.getElementById("page-mask").style.display="none";
	document.getElementById("forgot-div").style.display="none"; */
	document.getElementById("loginForm").style.display="block";
	document.getElementById("forgotForm").style.display="none";
	document.getElementById("registerButton").style.display="block";
	document.getElementById("forgotButton").style.display="block";
}
function forgotValidation() {
	var email=document.getElementById("forgotEmail").value; 
	var username=document.getElementById("forgotUser").value;
	if (!email && !username) {
		alert("Please enter values");
		return false;
	}
	if (email) {
		var atpos=email.indexOf("@"); 
		var dotpos=email.lastIndexOf(".");
		if (atpos<0 || atpos>dotpos) {
			// this is not a valid email address
			alert("Please enter a valid email address");
			document.getElementById("forgotEmail").value="";
			document.getElementById("forgotEmail").focus();
			return false;
		}
	}
	return true;
}


function showTerms() {
	document.getElementById("page-mask").style.display="block";
	document.getElementById("reg-div").style.display="none";
	document.getElementById("agree-div").style.display="block";
	document.getElementById("btn-agree").focus();
}
function doRegister() {
	var formObject = document.getElementById('regForm');
	document.getElementById('agreeToDataUseTerms').value="Y";
	YAHOO.util.Connect.setForm(formObject,false);
	var connectURI="/" + "registerUser";
	YAHOO.util.Connect.asyncRequest('POST',connectURI,regCallback,null);
}
function initRegisterCall() {
	var formObject = document.getElementById('regForm');
	YAHOO.util.Connect.setForm(formObject,false);
	var connectURI="/" + "registerUser";
	YAHOO.util.Connect.asyncRequest('POST',connectURI,initRegCallback,null);
}
function noThanks() {
	document.getElementById('agreeToDataUseTerms').value="N";
	regClose();
	ebNotify("You must agree to Data Use Terms");
}

/* Connectome Workbench Registration functions */

function showVersion(key) {
	var key="#"+key+"Version";
	$(".WBversion").addClass("hidden");
	$(key).removeClass("hidden");
}
function hideWBreg() {
	$("#wbreg").addClass("hidden");
	$("#defaultPanel").removeClass("hidden");	
}
function showWBreg() {
	$("#defaultPanel").addClass("hidden");
	$("#wbreg").removeClass("hidden");
}
function showRegThanks() {
	hideWBreg();
	$("#regThanks").removeClass("hidden");
}

function closeRegThanks() {
	$("#regThanks").addClass("hidden");
	$("#page-mask").addClass("hidden");
}
function closeRegCancel() {
	$("#regCancel").addClass("hidden");
}
function closeRegFail() {
	$("#regFail").addClass("hidden");
}
function cancelReg() {
	$("#regCancel").removeClass("hidden");
	hideWBreg();	
}
function showWBForgot() {
	$("#wbreg").addClass("hidden");
	$("#wbforgot").removeClass("hidden");
}

function closeWBforgot() {
	$("#wbforgot").addClass("hidden");
	$("#wbreg").removeClass("hidden");
}
function showUsername() {
	hideWBreg();
	$("#sentUsername").removeClass("hidden");
}
function closeUsername() {
	$("#sentUsername").addClass("hidden");
}

function showPassword() {
	hideWBreg();
	$("#sentPassword").removeClass("hidden");
}
function closePassword() {
	$("#sentPassword").addClass("hidden");
}
